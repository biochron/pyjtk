#!/usr/bin/env python

from setuptools import setup, find_packages

install_requires = ['pandas']

setup(
    name='pyjtk',
    version='1.0',
    package_dir={'': 'src'},
    packages=find_packages('src'),
    install_requires=install_requires,
    author="Alan Hutchinson (with minor edits by Francis Motta, Anastasia Deckard and Robert Moseley)",
    url='https://gitlab.com/biochron/pyjtk'
    )