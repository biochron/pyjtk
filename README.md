# pyJTK - Jonckheere-Terpstra Kendall Cycling Test in Python

This Python module implements the periodicity detection algorithm of Hughes et. al. (http://www.ncbi.nlm.nih.gov/pubmed/20876817) used to identify rhythmicity in gene expression profile time series data.

> **Disclaimer:**
> This package was written almost entirely by Alan Hutchison for use in Python 2 (see https://github.com/alanlhutchison/pyJTK).  It was modified slightly by Francis Motta and Anastasia Deckard as necessary for compatibility with Python 3 and to ensure consistency with the expected input data formats of other Data-to-Networks pipeline algorithms.

## Installing pyJTK

```
$ git clone git@gitlab.com:biochron/pyjtk.git
$ cd pyjtk
$ ./install.sh
```

## Running pyJTK
A typical run will be initiated by a terminal command:
```
$ python pyjtk.py <full path to data file> -T <periods of oscillation to test> -o <full path to directory to save output>
```

* **required command-line arguments**
    * **data_files**: full path(s) to gene expression data file(s)
    * **-T, --periods**: periods of oscillation to test
    * **-o, --output_file**: full path to file to save pyJTK output (should be .tsv or .txt file type)
* **optional command-line arguments**
    * **-h, --help**

### Example Command
```
$ python pyjtk.py /my_path_to_data/test_data_tps50_n25.txt -T "92, 96, 100, 104, 108" -o /my_path_to_output/pyjtk_test.tsv
```

### Data Input

The pyJTK module takes as input a single tab-delimited text file containing time series gene expression profiles, with rows indexed by gene names and columns indexed by time points. Replicates are handled by including columns indexed by teh same time point as shown.

|  | time 1 (rep 1) | time 1 (rep 2) | time 2 | ... | time n |
| :---: | :---: | :---: | :---: | :---: | :---: |
| **gene 1** | 653.13 | 648.22 | 578.55 | ... | 437.58 |
| **gene 2** | 226.35 | 231.25 | 254.71 | ... | 345.22 |
| ... | ... | ... | ... | ... | ... |
| **gene N** | 1274.12 | 1270.89 | 1124.77 | ... | 929.48 |

### Output
 
The pyJTK module outputs a tab delimited score file recording the corrected p-value, estimated amplitude, estimated period of cycling, estimated phase shift, and *tau value* for each gene:
 
|  | p-value | amp | period | lag | tau |
| :---: | :---: | :---: | :---: | :---: | :---: |
| **gene 1** | 0.11327 | 213 | 22 | 0.000024 | 0.027696  |
| **gene 2** | 0.05121 | 24 | 0.019 | 0.008664 | 652166.95 |
| ... | ... | ... | ... | ... | ... |
| **gene N** | 0.53548 | 24 | 0.771 | 0.68619 | 323098121452 |

## Author

* **Alan Hutchinson** (with minor edits by Francis Motta and Anastasia Deckard)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

#### Dependencies:
* Python (> 3.6.5)
* NumPy (> 1.14.3)
