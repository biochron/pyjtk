#!/usr/bin/python

import os.path
import sys
import pandas as pd

FPATH = os.path.dirname(os.path.realpath(__file__)),[0]
VERSION = "4.0"

import json
import unittest
import string
import argparse
import datetime
import time

from src.main import JTKCycleRun
from src.parsed import DataParser

import src.waveforms as w
import numpy as np

def main(args): # argument namespace

    if args.test:
        tests = unittest.defaultTestLoader.discover(FPATH[0]+"/spec",
                                                    pattern="*spec.py")
        test_runner = unittest.TextTestRunner(verbosity=1)
        test_runner.run(tests)
        return

    finput = args.data_file
    foutput = args.ofile
    fconfig = args.cfile

    parser = DataParser(finput, args.repattern)

    max_period = (args.max or 26) + 1
    min_period = args.min or 20
    period_step = args.period_step or 2

    try:
        periods = [ float(x) for x in args.periods.split(',')]
    except:
        periods = range(min_period, max_period, period_step)


    config = {}
    if fconfig is not None:
        config = json.load(fconfig)

    reps       = config.get("reps",        None) or parser.reps
    timepoints = config.get("timepoints",  None) or parser.timepoints
    periods    = config.get("periods",     None) or periods
    density    = config.get("offset_step", None) or args.offset_step

    function = __get_function__(args.function, args.width * np.pi * 2)
    symmetry = (args.function != "cosine") or args.symmetry

    if args.normal:
        distribution = "normal"
    else:
        distribution = "harding"

    start_time = datetime.datetime.now()
    test = JTKCycleRun(
        reps,
        timepoints,
        periods,
        density,
        distribution=distribution,
        function=function,
        symmetry=symmetry
        )

    end_time = datetime.datetime.now()

    summary = args.summary
    __write_comment__(foutput, end_time - start_time, args.data_file.name, periods)
    __write_header__(foutput, periods, summary)
    for name,series in parser.generate_series():
        _,_,_,_,_ = test.run(series)
        __write_data__(foutput, name, test, summary)

    # Variables are not currently used...
    p = args.pvalue

    finput.close()
    foutput.close()

    if args.qvalue:
        # Reload output file to compute Benjamini–Hochberg q-values
        if not summary:
            output_df = pd.read_csv(foutput.name, sep='\t', comment='#', index_col=0)
            output_df.sort_values('p-value', axis=0, ascending=True, inplace=True)
            output_df['rank'] = output_df['p-value'].rank(method='first').astype(int)
            output_df['probe'] = output_df.index
            output_df.index = output_df['rank']
            num_probes = len(output_df)
            output_df['adj.p'] = output_df['p-value']*num_probes/output_df['rank']
            output_df['q-value'] = np.nan
            output_df.loc[num_probes, 'q-value'] = output_df.loc[num_probes, :]['adj.p']
            for i in range(num_probes-1, 0, -1):
                output_df.loc[i, 'q-value'] = min(output_df.loc[i+1, 'q-value'], output_df.loc[i, 'adj.p'])

            output_df.index = output_df['probe']

            end_time = datetime.datetime.now()
            # Rewrite results to output
            with open(foutput.name, 'w', newline='\n') as f:
                __write_comment__(f, end_time - start_time, args.data_file.name, periods)
                output_df[['p-value', 'q-value', 'amp', 'period', 'lag', 'tau']].to_csv(f, sep='\t')

    if fconfig is not None:
        fconfig.close()

    return

def __get_function__(astr, width):
    f = np.cos
    if astr == "cosine":
        f = np.cos
    elif astr == "rampup":
        f = np.frompyfunc(
            lambda x: w.ramp_up(x, width),
            1, 1
            )
    elif astr == "rampdown":
        f = np.frompyfunc(
            lambda x: w.ramp_down(x, width),
            1, 1
            )
    elif astr == "impulse":
        f = np.frompyfunc(
            lambda x: w.impulse(x, width),
            1, 1
            )
    elif astr == "step":
        f = np.frompyfunc(
            lambda x: w.step(x, width),
            1, 1
            )
    else:
        f = np.cos
    return f

#
# printer utilities
#

def __write_comment__(foutput, run_time, in_file_name, periods):
    foutput.write('# pyJTK periodicity scores computed on %s\n' % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    foutput.write('# Calculation time: %f seconds\n' % run_time.total_seconds())
    foutput.write('# Input file: %s\n' % os.path.abspath(in_file_name))
    foutput.write('# Period(s) of oscillation tested: %s\n' % ', '.join(["{:.4f}".format(per) for per in periods]))

def __write_header__(foutput, periods, summary=False):
    if summary:
        foutput.write("id")
        for period in sorted(periods):
            foutput.write("\t" + str(period) + "-HR")
        else:
            foutput.write("\n")
    else:
        foutput.write("probe"+"\t"
                      +"p-value"+"\t"
                      +"amp"+"\t"
                      +"period"+"\t"
                      +"lag"+"\t"
                      +"tau"+"\n")
    return

def __write_data__(foutput, name, test, summary=False):
    if summary:
        foutput.write(name)
        est_amp,_,_,_,_ = test.best
        for period in sorted(test.results.keys()):
            offset, k_score, p_value = test.results[period]
            outstr = str(p_value)+";"+str(period)+";"+str(offset)+";"+str(est_amp)
            foutput.write("\t" + outstr)
        else:
            foutput.write("\n")
    else:
        est_amp, period, offset, k_score, p_value = test.best
        foutput.write(name+"\t"
                      +str(p_value)+"\t"
                      +str(est_amp)+"\t"
                      +str(period)+"\t"
                      +str(offset)+"\t"
                      +str(k_score)+"\n")
    return



#
# runner script utilities
#

def __create_parser__():

    def make_file_valid(file):
        if not os.path.exists(os.path.dirname(file)):
            os.makedirs(os.path.dirname(file))
        return open(file, 'w')

    p = argparse.ArgumentParser(
        description="python script runner for JTK_CYCLE statistical test"
        )

    p.add_argument("-t", "--test",
                   action='store_true',
                   default=False,
                   help="run the Python unittest testing suite")
    p.add_argument('data_file',
                    action='store',
                    help='file from which to read data',
                    type=argparse.FileType('r'))
    analysis = p.add_argument_group(title="JTK_CYCLE analysis options")
    analysis.add_argument("--function",
                          dest="function",
                          type=str,
                          metavar="$FUNC_STR",
                          action='store',
                          default="cosine",
                          choices=["cosine","rampup","rampdown","step","impulse"],
                          help="cosine (dflt), rampup, rampdown, impulse, step")
    analysis.add_argument("--assymetric",
                          dest="symmetry",
                          action="store_false",
                          default=True,
                          help="flag for half-density lags")
    analysis.add_argument("-w", "--width",
                          dest="width",
                          type=float,
                          metavar="W",
                          action='store',
                          default=0.75,
                          help="shape parameter for alt. waveforms \in [0,1]")
    analysis.add_argument("-p", "--pvalue",
                          metavar="P",
                          type=float,
                          default=0.01,
                          help="set p-value to define significance (dflt: 0.01)")


    distribution = analysis.add_mutually_exclusive_group(required=False)
    distribution.add_argument("-e", "--exact",
                              dest="harding",
                              action='store_true',
                              default=False,
                              help="use Harding's exact null distribution (dflt)")
    distribution.add_argument("-n", "--normal",
                              dest="normal",
                              action='store_true',
                              default=False,
                              help="use normal approximation to null distribution")
    
    periods = p.add_argument_group(title="JTK_CYCLE custom search periods")
    periods.add_argument("-T", "--periods",
                         metavar="$JSON_ARR",
                         type=str,
                         required=True,
                         action='store',
                         help="array specifying periods i.e. [1,3,5,7]")
    periods.add_argument("--min",
                         metavar="N",
                         type=int,
                         help="set min period to number N hours (dflt: 20)")
    periods.add_argument("--max",
                         metavar="N",
                         type=int,
                         help="set max period to number N hours (dflt: 26)")
    periods.add_argument("--period-step",
                         metavar="N",
                         type=int,
                         help="determines range step N in hours (dflt: 2)")
    periods.add_argument("--offset-step",
                         metavar="N",
                         type=float,
                         action='store',
                         help="offset step size N of half-hours (dflt: 2)")

    parser = p.add_argument_group(title="parser option")
    parser.add_argument("-r", "--repattern",
                        action='store_true',
                        default=False,
                        help="use header to re-order data series by ZT")
    parser.add_argument("-q", "--qvalue",
                        action='store_true',
                        default=False,
                        help="flag to compute BH q-value")

    files = p.add_argument_group(title="files & output management options")
    files.add_argument("-i", "--input",
                       dest="ifile",
                       metavar="FILENM",
                       default="-",
                       type=argparse.FileType('r'),
                       help="file from which to read data (dflt: stdin)")
    files.add_argument("-o", "--output",
                       dest="ofile",
                       metavar="FILENM",
                       type=lambda x: make_file_valid(x),
                       default="-",
                       help="file to write results (dflt: stdout)")
    files.add_argument("-c", "--config",
                       dest="cfile",
                       metavar="FILENM",
                       type=argparse.FileType('r'),
                       help="read {reps,times,periods,density} from JSON")

    printer = p.add_argument_group(title="result output preferences")
    printer.add_argument("-s", "--summary",
                         action='store_true',
                         default=False,
                         help="print summary over all searched periods")

    return p

if __name__ == "__main__":
    parser = __create_parser__()
    args = parser.parse_args()
    main(args)
